# 27/03/2017 XL
# main script for generating data, running networks, and calculating parameters
# by GD. Network performance is then saved to file.

import numpy as np
import generateData as gd
import train_network as train_net
import network_derivatives as deriv

# paths
folder = '/home/darwin/Documents/Analysis/bayesian_deeplearning/'
traindatapath = folder+'Data/train/'
modelpath = folder+'modelWeights/best_weights'
losspath = folder+'trainingLoss/loss'
testdataPath = folder+'Data/test/'
GDpath = folder+'GDresults/resultsModel'
# parameters 
iterModels = 1 # number of model iterations
trainingSets = [10, 50, 100, 200, 500, 1000]
seed = 1234
timesteps = 10   
num_states  = 2 
num_obs = 2 
dt = 1.0 
nonlinear_fxn = np.exp # nonlinearity
noise_const = 1.0 # for H
minEig = -0.10
maxEig = 0.0 # lower/upper bound for eig(F)
qRange = 3 # -/+ range for values in Q covariance
rRange = 3 # -/+ range for values in R covariance 
patience = 30  # number of epochs with no improvement before training stops 
n_epochs = 1000 # TODO times to go through all training data
hidden_units = 50 
# gradient descent parameters
em_cycles = 5 
momentum_rate = 0.5 
learning_rate = 0.5
convergence = 1e-4
GD_epochs = 500 
# output list 
lossHistory = [] 
filter_mse = np.zeros([iterModels]) 
emfilter_mse = np.zeros([iterModels]) 
ukfilter_mse = np.zeros([iterModels])
nn_mse = np.zeros([iterModels])
obs_mse = np.zeros([iterModels])
naive_mse = np.zeros([iterModels])
# loop model iterations
# TODO: double check or remove hardcode for obs indices
for dataSize in trainingSets:
    for iterModel in range(iterModels):
        # update model save path
        curr_modelpath = modelpath + '_modelIter' + str(iterModel) + '_'
        # increment seed
        np.random.seed(seed+iterModel)
        # generate train data
        gd.generateData(folder,iterModel,nonlinear_fxn,minEig,maxEig,qRange,rRange,dataSize,timesteps,num_states,num_obs,dt,noise_const)
        # train network until convergence
        lossHistory.append( train_net.train(iterModel,dataSize,patience,n_epochs,timesteps,hidden_units,num_obs,traindatapath,curr_modelpath,losspath) )
        # call network_derivatives.py to find params by GD and mse 
        filter_mse[iterModel],emfilter_mse[iterModel],nn_mse[iterModel],ukfilter_mse[iterModel],obs_mse[iterModel], naive_mse[iterModel]=\
        deriv.param_GD(dataSize,em_cycles,timesteps,hidden_units,iterModel,curr_modelpath,testdataPath,GDpath,momentum_rate,learning_rate,convergence,GD_epochs,nonlinear_fxn)
