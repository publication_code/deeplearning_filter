# implements kalman, extended, unscented, particle filters 

from numpy.linalg import norm
from numpy.random import randn
import scipy.stats
from numpy.random import uniform

def kalman(dims,init_state,init_sigma,obs,states,F,H,P,R,Q):
    f = kf(dim_x=states.shape[1],dim_z=obs.shape[1]) # x: states, z: obs
    f.x = init_state # init state
    f.F = F # transition matrix
    f.H = H # measurement matrix
    f.P = init_sigma # error covariance matrix
    f.R = R # measurement noise
    f.Q = Q # process noise 
    # predict/update loop
    for curr_obs in obs:
        f.predict() # predict: before measurement
        f.update(curr_obs) # update: after measurement
        # store predictions after each step        

def extended(dims,init_state,init_sigma,obs,states,F,H,P,R,Q):
    f = extended_filter(dim_x=states.shape[1],dim_z=obs.shape[1])
    f.x = init_state 
    f.P = init_sigma 
    f.R = R # measurement noise
    f.Q = Q # process noise


    # transition matrix; measurement matrix; process noise?
    for curr_states,curr_obs in obs,states:
            # update substitutions
            f.subs[s1]=curr_states[0]
            f.subs[s2]=curr_states[1]
            f.subs[o1]=curr_obs[0]
            f.subs[o2]=curr_obs[1]
            f.predict() # states: x_k | x_k-1
            f.update(curr_obs,R=self.R,HJacobian=f.HJacobian,Hx=f.nonlinearH,args=(self.nonlinearH),
                    hx_args=(self.nonlinearH))

def unscented:
    pass


def particle:
    # generate particles 
    particles = create_uniform_particles()
    
    # predict next particle states
    # update particle weightings with measurement
    # resample
    # estimate weighted mean and covariance

def create_uniform_particles(x_range, y_range, hdg_range, N):
    particles = np.empty((N, 3))
    particles[:, 0] = uniform(x_range[0], x_range[1], size=N)
    particles[:, 1] = uniform(y_range[0], y_range[1], size=N)
    particles[:, 2] = uniform(hdg_range[0], hdg_range[1], size=N)
    particles[:, 2] %= 2 * np.pi
    return particles

def create_gaussian_particles(mean, std, N):
    particles = np.empty((N, 3))
    particles[:, 0] = mean[0] + (randn(N) * std[0])
    particles[:, 1] = mean[1] + (randn(N) * std[1])
    particles[:, 2] = mean[2] + (randn(N) * std[2])
    particles[:, 2] %= 2 * np.pi
    return particles

def predict(particles, u, std, dt=1.):
    """ move according to control input u (heading change, velocity)
    with noise Q (std heading change, std velocity)`"""

    N = len(particles)
    # update heading
    particles[:, 2] += u[0] + (randn(N) * std[0])
    particles[:, 2] %= 2 * np.pi

    # move in the (noisy) commanded direction
    dist = (u[1] * dt) + (randn(N) * std[1])
    particles[:, 0] += np.cos(particles[:, 2]) * dist
    particles[:, 1] += np.sin(particles[:, 2]) * dist

def update(particles, weights, z, R, landmarks):
    # sequential importance sampling ~ likelihood
    weights.fill(1.)
    for i, landmark in enumerate(landmarks):
        distance = np.linalg.norm(particles[:, 0:2] - landmark, axis=1)
        weights *= scipy.stats.norm(distance, R).pdf(z[i])

    weights += 1.e-300      # avoid round-off to zero
    weights /= sum(weights) # normalize

def estimate(particles, weights):
    """returns mean and variance of the weighted particles"""
    pos = particles[:, 0:2]
    mean = np.average(pos, weights=weights, axis=0)
    var  = np.average((pos - mean)**2, weights=weights, axis=0)
    return mean, var

def resample_from_index(particles, weights, indexes):
    particles[:] = particles[indexes]
    weights[:] = weights[indexes]
    weights /= np.sum(weights) # normalize


def run(states,obs,particles=1000,iters=18,sensor_std_err=.1,init_x=None):
    # create particles and weights
    if init_x is not None:
        particles = create_gaussian_particles(mean=initial_x, std=(5, 5, np.pi/4), N=N)
    else:
        particles = create_uniform_particles((0,20), (0,20), (0, 6.28), N)
    weights = np.zeros(N)
   
    xs = obs
    robot_pos = np.array([0., 0.])
    for x in range(iters):
        # observations
        zs = 
        # move diagonally forward to (x+1, x+1)
        predict(particles, u=(0.00, 1.414), std=(.2, .05))
        # incorporate measurements
        update(particles, weights, z=zs, R=sensor_std_err, 
               landmarks=landmarks)
        # resample if too few effective particles
        if neff(weights) < N/2:
            indexes = systematic_resample(weights)
            resample_from_index(particles, weights, indexes)
        mu, var = estimate(particles, weights)
        xs.append(mu)
    xs = np.array(xs)

class extended_filter(ekf):
    def __init__(self):
        ekf.__init__(self,dim_x,dim_z,dim_u=0)
        # symbolic vars
        s1,s2,o1,o2 = sym.symbols('state1,state2,obs1,obs2')
        self.nonlinearF = sym_exp(sym.Matrix( 
            [[s1],
             [s2]]))
       self.FJacobian = self.nonlinearfunF.jacobian(sym.Matrix([s1,s2])) # linearized
        self.nonlinearH = sym_exp(sym.Matrix(
            [[s1],
             [s2]]))
        # substitution dictionary
        self.subs = {s1: 0, s2: 0, o1:0, o2:0}

    def predict(self,nonlinear_fxn,u=0):
        # substitute jacobian
        F = array(self.FJacobian.evalf(subs=self.subs)).astype(float) # df(x)/dx
        # predict state
        self.x = self.x + np.dot(F,self.x) # x_new = F*x
        # predict errror covariance 
        self.P = dot(F, self.P).dot(F.T) + self.Q # P_new=FPF.t+ Q 
    
    def HJacobian(x,nonlinearfunH):
        '''
        Jacobian of the H matrix (measurement function). 
        Takes state variable (self.x) as input, returns H.
         '''
        HJacobianMat = nonlinearfunH.jacobian(sym.Matrix([s1,s2])) # linearized
        return np.array(HJacobianMat.evalf(subs={x1:x[0],x2:x[1])).astype(float)

    def Hx(x, nonlinearfunH):
        """ 
        takes a state variable and returns the measurement
        that would correspond to that state.
        """
        Hx = np.array(nonlinearfunH.evalf(subs={x1:x[0],x2:x[1])).astype(float) 
        return Hx

import numpy as np
from scipy import linalg

class filter:
    ''' base class for different filters'''
    def __init__(self,transitionMat=None,obsMat=None,transitionCov=None,
        obsCov=None,initMean=None,initCov=None,numStates=None,numObs=None,
        em_vars = ['transCov', 'obsCov','initMean','initCov']):

        # initialize filter variables        
        self.transition_function = transMat 
        self.observation_function= obsMat 
        self.transition_covariance = transitionCov 
        self.observation_covariance = obsCov 
        self.initial_state_mean = initMean 
        self.init_covariance = initCov 
        self.em_vars = em_vars
        self.numStates = numStates 
        self.numObs = numObs 

    def _log_likelihood():

    def _predict():

    def _update():

    def _expectation_max():

    def filter(self, observations):
        # apply filtering on given observations to estimate hidden variables
        # gives estimate of state at t given state estimate for time t-1 
        # and observations from [0:t]
        # observations : [timesteps, numStates]

        (transition_matrices, transition_offsets, transition_covariance,
         observation_matrices, observation_offsets, observation_covariance,
         initial_state_mean, initial_state_covariance) = (
            self._initialize_parameters()
        )

        (_, _, _, filtered_state_means,filtered_state_covariances) = 
            _filter # TODO
            (
                transition_matrices, observation_matrices,
                transition_covariance, observation_covariance,
                transition_offsets, observation_offsets,
                initial_state_mean, initial_state_covariance,
                observations
            )
        
    return filtered_state_means, filtered_state_covariances


