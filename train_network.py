# -*- coding: utf-8 -*-
"""
Created on Fri Jan 5th 2017 

LSTM network to learn Kalman filter
Uses many-to-many architecture

@author XL
"""

import numpy as np
import math
import pickle 
import glob
import sys
import os
import theano
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, TimeDistributed, Dropout, LSTM, GRU
from keras.optimizers import SGD
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping
from keras import backend as K
import generateData as gd
from operator import itemgetter

'''
# print out values/weights from layers
model.layers[0].get_weights()
model.layers[0].get_config()
layer.input
layer.output
layer.input_shape
layer.output_shape

layers can have multiple i/o nodes
model.shape_output
model.layers[0].trainable_weights -> list of weight names corresponding to get_weights()
model.layers[0].states[0].get_value() -> [h,c]
'''

class LossHistory(Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))

def layer_output(layer_in,layer_out,input_data):
    get_layer_output = K.function([model.layers[layer_in].input],[model.layers[layer_out].output])
    # NB: need to pass K.learning_phase() if using different behavior, =0 in test mode
    return get_layer_output([input_data])[0]

def split_dataset(dataset,hold_out=0.1,splitdim=0):    
    # split into train and test sets (this is usu. cross-validation in regression probs)
    train_size = int(dataset.shape[splitdim] * (1-hold_out)) 
    train, test = dataset[0:train_size],dataset[train_size:]
    return train, test

# convert an array of values into a dataset matrix
def stagger_dataset(X,Y,dim=-2,timesteps=1):
    dataX,dataY = [],[] 
    for h in range(X.shape[0]): # datasets
        # iterate over 2nd dim to form staggerred ts
        for i in range(X.shape[dim]-timesteps-1): # timesteps
            sample = X[h,i:(i+timesteps),:] # (datasets,timesteps,features) 
            dataX.append(sample)
            dataY.append(Y[h,(i+timesteps),:]) # predicted state for final timestep 
    return np.array(dataX), np.array(dataY)

def flatten_params(params):
    flattened_params = np.empty([1,]) 
    for item in params:
        if isinstance(item,np.ndarray):
            flattened_params = np.append(flattened_params,item.flatten())
        elif type(item) is int:
            flattened_params = np.append(flattened_params,item)
        elif item is None:
            pass
        else:
            print('type not recognized')
    flattened_params = np.delete(flattened_params,[0])
    return flattened_params

def input_output (layer_index,X): # layer idx, test input
    get_layer_output = K.function([model.layers[layer_index].input], [model.layers[layer_index].output])
    layer_output = get_layer_output([X])[0]
    return (X,layer_output)

def load_parameters(name):
    with open(name,'rb') as dataf:
        param_set = pickle.load(dataf)
        datasets = param_set['datasets'] 
        totalsteps = param_set['totalsteps'] # total timesteps for each param set
        features = param_set['features'] # trainX.shape[1]
        states = param_set['states']
    X = np.empty((datasets,totalsteps,features)) 
    Y = np.empty((datasets,totalsteps,states))
    return X, Y, states, features 

def load_datasets(X_datamatrix, Y_datamatrix,datapath,parampath):
    data_files = glob.glob(datapath)
    data_files.remove(parampath)
    for i, filename in enumerate(data_files):
        F,H,Q,R,G,u,init_state,init_sigma,tsteps,states,obs= gd.load_data(filename) 
        # reshape data into (m,) vector 
        input_params = flatten_params([F,H,Q,R,G]) # exclude u,init_state,tsteps,states,obs
        input_params_tiled = np.tile(input_params,(tsteps,1)) # (tsteps,params)
        input_addObs= np.concatenate([input_params_tiled,obs.T],axis=1) # (tsteps,params+obs)
        input_combined = np.concatenate([input_addObs,u.T],axis=1) # (tsteps,params+u)
        output_y = states.T # states (tsteps,states)
        # append to data matrices 
        X_datamatrix[i,:,:] = input_combined # input_normd 
        Y_datamatrix[i,:,:] = output_y   
    return X_datamatrix, Y_datamatrix, tsteps

def reshape_data(X_datamatrix,Y_datamatrix,holdout=0.1,timesteps=1):
    # split dataset into test and training sets along 1st dimension 
    trainX, valX = split_dataset(X_datamatrix,hold_out=0.1)  
    trainY, valY = split_dataset(Y_datamatrix,hold_out=0.1)       
    return trainX,valX,trainY,valY

def train(iterModel,dataSize,patience,n_epochs,timesteps,hidden_units,numObs,datapath,modelpath,losspath):

    # define save path
    bestmodel_path = modelpath + 'mainmodel' + '_' + str(dataSize) + '.hf5'
    obsmodel_path = modelpath + 'obsmodel' + '_' + str(dataSize) + '.hf5'
    # load data
    parampath= datapath + 'set' + str(iterModel) + '/params.dat' # full name of parameter file
    fulldatapath = datapath + 'set' + str(iterModel) + '/*.dat'
    X_empty, Y_empty, states, features = load_parameters(parampath)
    X_datamatrix,Y_datamatrix,totalsteps = load_datasets(X_empty,Y_empty,fulldatapath,parampath)
    trainX,valX,trainY,valY = reshape_data(X_datamatrix,Y_datamatrix,holdout=0.1,timesteps=totalsteps)
    # parameter indices
    param_indices = [i for i in range(trainX.shape[2])]
    param_indices[20:22] = [] # obs_indices
    obs_indices = [20,21] # observation indices
 
    # training parameters
    batch_samples = trainX.shape[0] # number of samples per batch
    features = trainX.shape[-1]
    output = trainY.shape[-1]
    timesteps = trainX.shape[-2] # number of steps per sample 
    trainXobs = trainX[:,:,obs_indices]
    valXobs = valX[:,:,obs_indices]

    # build main model
    model = Sequential()
    model.add(LSTM(hidden_units,input_shape=(timesteps,features),return_sequences=True)) # timesteps,features 
    model.add(LSTM(hidden_units,return_sequences=True))
    model.add(TimeDistributed(Dense(output,init='uniform',activation='linear')))
    model.compile(loss='mse',optimizer='rmsprop')   
    # build obs-only model
    obsmodel = Sequential()
    obsmodel.add(LSTM(hidden_units,input_shape=(timesteps,numObs),return_sequences=True)) # timesteps,features 
    obsmodel.add(LSTM(hidden_units,return_sequences=True))
    obsmodel.add(TimeDistributed(Dense(output,init='uniform',activation='linear')))
    obsmodel.compile(loss='mse',optimizer='rmsprop')   
   
    # callbacks main model
    history_model = LossHistory()
    earlystop=EarlyStopping(monitor='loss',patience=patience,verbose=0,mode='auto') 
    checkpointer = ModelCheckpoint(filepath=bestmodel_path,verbose=1,save_best_only=True)
    # callbacks obs only model
    history_obsmodel = LossHistory()
    obscheckpt = ModelCheckpoint(filepath=obsmodel_path,verbose=1,save_best_only=True)
 
    # fit model
    modelHistory=model.fit(trainX,trainY,epochs=n_epochs,batch_size=batch_samples,verbose=1,shuffle=True,callbacks=[checkpointer,earlystop],validation_data=(valX,valY))
    # fit obs only model
    obsHistory = obsmodel.fit(trainXobs,trainY,epochs=n_epochs,batch_size=batch_samples,verbose=1,shuffle=True,callbacks=[obscheckpt,earlystop],validation_data=(valXobs,valY))
    # save losses to file 
    np.save(losspath+str(iterModel)+'.npy',modelHistory.history,obsHistory.history)
    return modelHistory.history
