# -*- coding: utf-8 -*-
"""
Extract derivatives with respect to parameters from 
Keras network model

@author: XL 
"""

import numpy as np
import scipy as sp
import math
import glob
import pickle 
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, TimeDistributed, Dropout, LSTM, GRU
from keras import backend as k # theano backend
import generateData as gd
import sys
from pykalman import KalmanFilter,UnscentedKalmanFilter, AdditiveUnscentedKalmanFilter
import theano
import theano.tensor as T 

'''
# print out values/weights from layers
model.layers[0].get_weights()
model.layers[0].get_config()
layer.input
layer.output
layer.input_shape
layer.output_shape

layers can have multiple i/o nodes
model.shape_output
model.layers[0].trainable_weights -> list of weight names corresponding to get_weights()
model.layers[0].states[0].get_value() -> [h,c]
'''

def split_dataset(dataset,hold_out=0.1,splitdim=0):    
    # split into train and test sets (this is usu. cross-validation in regression probs)
    train_size = int(dataset.shape[splitdim] * (1-2*hold_out)) 
    val_size = int(dataset.shape[splitdim] * hold_out)
    train, val, test = dataset[0:train_size],dataset[train_size:-val_size],dataset[train_size+val_size:]
    return train, val, test

def flatten_params(params):
    flattened_params = np.empty([1,]) 
    for item in params:
        if isinstance(item,np.ndarray):
            flattened_params = np.append(flattened_params,item.flatten())
        elif type(item) is int:
            flattened_params = np.append(flattened_params,item)
        elif item is none:
            pass
        else:
            print('type not recognized')
    flattened_params = np.delete(flattened_params,[0])
    return flattened_params

def load_parameters(name):
    with open(name,'rb') as dataf:
        param_set = pickle.load(dataf)
        datasets = param_set['datasets'] 
        totalsteps = param_set['totalsteps'] # total timesteps for each param set
        features = param_set['features'] # trainx.shape[1]
        states = param_set['states']
    x = np.empty((datasets,totalsteps,features)) 
    y = np.empty((datasets,totalsteps,states))
    return x, y, states, features 

def reshape_data(x_datamatrix,y_datamatrix,holdout=0.1,timesteps=1):
    trainx, valx, testx = split_dataset(x_datamatrix,hold_out=holdout)  
    trainy, valy, testy = split_dataset(y_datamatrix,hold_out=holdout)       
    return trainx,valx,testx,trainy,valy,testy

def load_datasets(x_datamatrix, y_datamatrix,datapath,parampath):
    data_files = glob.glob(datapath)
    data_files.remove(parampath)
    for i, filename in enumerate(data_files):
        f,h,q,r,g,u,init_state,init_sigma,tsteps,states,obs,true_params=gd.load_test(filename) 
        # reshape data into (m,) vector 
        input_params = flatten_params([f,h,q,r,g]) # exclude u,init_state,tsteps,states,obs
        input_params_tiled = np.tile(input_params,(tsteps,1)) # (tsteps,params)
        input_addobs= np.concatenate([input_params_tiled,obs.T],axis=1) # (tsteps,params+obs)
        input_combined = np.concatenate([input_addobs,u.T],axis=1) # (tsteps,params+u)
        output_y = states.T # states (tsteps,states)
        # append to data matrices 
        x_datamatrix[i,:,:] = input_combined # input_normd 
        y_datamatrix[i,:,:] = output_y   
    return x_datamatrix,y_datamatrix,tsteps,true_params,init_state,init_sigma,f,h,q,r,g,u

def update_parameters(updated_x,grad_params,param_indices,tsteps,numsamples):
    # update parameter indices
    tiled_params = np.tile(grad_params,(numsamples,tsteps,1))
    updated_x[:,:,param_indices] -= tiled_params[:,:,param_indices]
    return updated_x 

def reshape_timeseries(obs):
    # join training samples into a single series
    obs_reshaped = obs.reshape(obs.shape[0]*obs.shape[1],obs.shape[2])
    return obs_reshaped

def assemble_parameters(param_vec,param_indices):
    # reassemble parameters into dictionary from feature vector
    u = param_vec[-2:] 
    f = param_vec[:4].reshape((2,2),order='c')
    h = param_vec[4:8].reshape((2,2),order='c')
    q = param_vec[8:12].reshape((2,2),order='c')
    r = param_vec[12:16].reshape((2,2),order='c')
    g = param_vec[16:20].reshape((2,2),order='c') 
    param_dict = {'f':f,'h':h,'q':q,'r':r,'g':g,'u':u}
    return param_dict

# functions for extracting gradients
def layer_output(model, layer):
    layer_output=theano.function([model.layers[0].input],model.layers[layer].output,allow_input_downcast=True)
    # k.learning_phase
    return layer_output # returns output from layer

def layer_gradient(model,out_layer,learning_rate):
    target = T.tensor3('target') # auto-typed to floatx 
    x = model.layers[0].input # inputs: obs + params
    y = model.layers[out_layer].output
    # cost = T.mean(T.pow(target-y,2)) # mse
    cost = T.mean(T.sqr(target - y)) 
    gradients = T.grad(cost, x)   
    # update params 
    # params_updated = params - (learning_rate * gradients) 
    # updates = [(params, t.set_subtensor(x[blah],params_updated)] # list of update tuples
    # updates = [(params, params_updated)] 
    # gradient descent step
    grad_descent=theano.function([x,target],gradients,allow_input_downcast=True)
    cost_fxn = theano.function([y,target],cost,allow_input_downcast=True)
    return grad_descent, cost_fxn

# unscented kalman filter function 
def build_ukf_fxn(nonlinear_fxn,linearmatrix):
    def ukf_fxn(state):
        new_state=nonlinear_fxn(np.dot(linearmatrix,state))
        return new_state
    return ukf_fxn

def param_GD(dataSize,em_cycles,steps_per_sample,hidden_units,modelNum,modelpath,testdataPath,GDpath,momentum_rate,learning_rate,convergence,totalEpochs,nonlinear_fxn):
    # use these to update params in GD function
    # updates.append((W, T.set_subtensor(W[:, 0], W[:, 0]-learning_rate*gradient_W)))
    # updates.append((W, T.inc_subtensor(W[:, 0], -learning_rate*gradient_W)))

    # full path names
    parampath= testdataPath + 'set' + str(modelNum) + '/params.dat' # full name of parameter file
    fulldatapath = testdataPath + 'set' + str(modelNum) + '/*.dat'
    # load NN model
    network = load_model(modelpath + 'mainmodel' +'_' +  str(dataSize) + '.hf5') 
    obsmodel = load_model(modelpath + 'obsmodel' + '_' + str(dataSize) + '.hf5')
    print('Loaded network model %i' % modelNum)
    # load data
    X_empty, Y_empty, states, features = load_parameters(parampath)
    X_datamatrix,Y_datamatrix,totalsteps,true_params,init_state,init_sigma,F_avg,H_avg,Q_avg,R_avg,G_avg,u_avg=load_datasets(X_empty,Y_empty,fulldatapath,parampath)
    trainX,valX,testX,trainY,valY,testY = reshape_data(X_datamatrix,Y_datamatrix,holdout=0.1,timesteps=steps_per_sample)
    # parameter indices that require updating 
    param_indices = [i for i in range(trainX.shape[2])]
    param_indices[20:22] = [] # obs_indices
    obs_indices = [20,21] # observation indices
    observations = trainX[:,:,obs_indices]
    num_samples = trainX.shape[0]
    val_samples = valX.shape[0]
    test_samples = testX.shape[0]
    # build gradient descent function 
    epochLoss = 1e3 # init epoch loss
    numEpochs = 0 # init epoch counter
    grad_descent, cost = layer_gradient(network, -1, learning_rate) 
    output_fxn = layer_output(network,-1)
    # initiate loss, grad vec, and feature matrix used in loop
    deltaLoss = 1000.0
    valLoss = 1000.0
    grad_params = np.zeros(trainX.shape[2])
    updated_trainX = trainX.copy()
    updated_valX = valX.copy()
    updated_testX = testX.copy()
    # mse loss vectors
    mseTestVec = np.zeros(totalEpochs)
    mseValVec = np.zeros(totalEpochs)
    mseTrainVec = np.zeros(totalEpochs)
    # loop grad_descent
    while (numEpochs < totalEpochs) and (deltaLoss > convergence):
        # sum gradients on inputs 
        gradients_inputs = grad_descent(updated_trainX,trainY) # inputs, target; outputs=gradients
        sum_sample_gradients = np.sum(gradients_inputs,axis=0) 
        sum_sampletime_gradients = np.sum(sum_sample_gradients,axis=0)
        # momentum gradient 
        grad_params = momentum_rate * grad_params + learning_rate * sum_sampletime_gradients 
        # update parameters and recreate input samples; done in-loop because need to update grad
        updated_trainX=update_parameters(updated_trainX,grad_params,param_indices,steps_per_sample,num_samples) 
        updated_valX=update_parameters(updated_valX,grad_params,param_indices,steps_per_sample,val_samples) 
        updated_testX = update_parameters(updated_testX,grad_params,param_indices,steps_per_sample,test_samples) 
        # print change in objective fxn
        outputs = output_fxn(updated_trainX)
        val_outputs = output_fxn(updated_valX)
        old_valLoss = valLoss
        epochLoss = cost(outputs,trainY) # y_hat, target
        valLoss = cost(val_outputs,valY)
        deltaLoss = np.abs(old_valLoss - valLoss) 
        print('Epoch','\t training loss', '\t\t validation loss')
        print(numEpochs,'\t',epochLoss,'\t',valLoss) 
        # save epochLoss and test performance
        mseValVec[numEpochs] = valLoss 
        mseTrainVec[numEpochs] = epochLoss
        numEpochs += 1   
    # join obs and states
    F_true,H_true,Q_true,R_true,G_true,u_true = map(true_params.get,('F_true','H_true','Q_true','R_true','G_true','u_true'))
    observations = reshape_timeseries(trainX[:,:,obs_indices])
    states_joined = reshape_timeseries(trainY)
    test_observations = reshape_timeseries(testX[:,:,obs_indices])
    test_states_joined = reshape_timeseries(testY) 
    # Kalman filter oracle
    kf_true = KalmanFilter( 
    transition_matrices=F_true,
    observation_matrices=H_true,
    transition_covariance=Q_true, 
    observation_covariance=R_true, 
    # transition_offsets=u, # offset to state mean
    # observation_offset=d, #  
    initial_state_mean=init_state, 
    initial_state_covariance=init_sigma,
    ) 
    # Kalman filter EM
    kf_em = KalmanFilter( 
    transition_matrices=F_avg,
    observation_matrices=H_avg,
    transition_covariance=Q_avg, 
    observation_covariance=R_avg, 
    # transition_offsets=u, # offset to state mean
    # observation_offset=d, #  
    initial_state_mean=init_state, 
    initial_state_covariance=init_sigma,
    em_vars=[
    'transition_matrices',
    'observation_matrices',
    'transition_covariance',
    'observation_covariance'])
    # 'initial_state_mean',
    # 'initial_state_covariance']) # opt: vars to be estimated by EM
    # Unscented Kalman filter
    ukf_transition = build_ukf_fxn(nonlinear_fxn,F_true)
    ukf_observation = build_ukf_fxn(nonlinear_fxn,H_true)
    ukf_true = AdditiveUnscentedKalmanFilter( 
    transition_functions=ukf_transition, 
    observation_functions=ukf_observation,
    transition_covariance=Q_true, 
    observation_covariance=R_true, 
    initial_state_mean=init_state, 
    initial_state_covariance=init_sigma,
    ) 
    # run EM
    em_loglik= np.zeros(em_cycles)
    for i in range(len(em_loglik)):
        print('EM cycle %d' %i)
        kf_em = kf_em.em(X=observations, n_iter=1) 
        em_loglik[i] = kf_em.loglikelihood(observations) # corrected source code

    # build naive (untrained) model
    naivemodel = Sequential()
    naivemodel.add(LSTM(hidden_units,input_shape=(steps_per_sample,features),return_sequences=True)) 
    naivemodel.add(LSTM(hidden_units,return_sequences=True))
    naivemodel.add(TimeDistributed(Dense(trainY.shape[-1],init='uniform',activation='linear')))
    naivemodel.compile(loss='mse',optimizer='rmsprop')   
   
    # run models on test data
    state_est_nn = network.predict(updated_testX) # np.array_equal: test_output = output_fxn(testX) # (100,100,24)
    state_est_obs = obsmodel.predict(updated_testX[:,:,obs_indices])
    state_est_naive = naivemodel.predict(updated_testX)
    state_est_kf,cov_est_kf = kf_true.filter(test_observations)  
    state_est_em, cov_est_em = kf_em.filter(test_observations)
    state_est_ukf, cov_est_ukf = ukf_true.filter(test_observations) # TODO: ukf EM, particle filter
    # calculate MSE
    filter_mse = np.mean( np.square(test_states_joined - state_est_kf) )
    emfilter_mse = np.mean( np.square(test_states_joined - state_est_em) )
    ukfilter_mse = np.mean( np.square(test_states_joined - state_est_ukf) )
    nn_mse = np.mean( np.square(testY - state_est_nn) ) 
    obs_mse = np.mean( np.square(testY - state_est_obs) )
    naive_mse = np.mean( np.square(testY - state_est_naive) )
    # compare parameters from EM and GD
    F_em = kf_em.transition_matrices
    H_em = kf_em.observation_matrices
    Q_em = kf_em.transition_covariance
    R_em = kf_em.observation_covariance
    # em_features  
    nn_features = updated_trainX[0,0,param_indices] # take feature vector sans obs 
    real_features = flatten_params([F_true,H_true,Q_true,R_true,G_true])
    real_features = np.concatenate([real_features,u_true.T[0,:]]) # append u
    nn_params = assemble_parameters(nn_features,param_indices) # changed from final_features
    # em_params = assemble_params(em_features,param_indices)
    real_params = assemble_parameters(real_features,param_indices) # should get back matrix_true
    # pickle parameters
    GD_results= {'filter_mse':filter_mse,'emfilter_mse':emfilter_mse,'ukfilter_mse':ukfilter_mse,'nn_mse':nn_mse,'nn_param_predict':nn_params,'real_params':real_params}
    with open(GDpath+str(modelNum),'wb') as f:
        pickle.dump(GD_results, f) 
    return (filter_mse, emfilter_mse, nn_mse, ukfilter_mse, obs_mse, naive_mse)
